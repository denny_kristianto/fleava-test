package denny.kristianto.fleavatest.utils;

/**
 * FleavaTest
 * Created by Denny Kristianto on 06/11/2018.
 * Copyright (c) 2018 by Denny Kristianto
 */
class Config {
    static final String SERVER = "http://128.199.83.121:5021/";
    static final String SHARED_PREF_CONFIG="fleava_config";
    static final String MOCK_SERVER="http://www.mocky.io/";
    static final String MOCK_ADDRESS="/v2/5be3081b2f00006b00ca221e";
}
