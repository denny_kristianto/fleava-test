package denny.kristianto.fleavatest.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

/**
 * FleavaTest
 * Created by Denny Kristianto on 06/11/2018.
 * Copyright (c) 2018 by Denny Kristianto
 */
public class StaticUtils {

    public static ApiInterface apiInterface= RetrofitClient.getClient(Config.SERVER).create(ApiInterface.class);
    public static ApiInterface mockInterface=RetrofitClient.getClient2(Config.MOCK_SERVER).create(ApiInterface.class);
    public static Map getHeader(Context context){
        Map<String,String> map=new HashMap<>();
        map.put("Accept","application/json");
        map.put("Authorization","Bearer "+ getSharedPreference(context, "token"));
        return map;
    }

    public static void writeSharedPreference(Context context,String key,String value){
        SharedPreferences sharedPref=context.getSharedPreferences(Config.SHARED_PREF_CONFIG,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key,value);
        editor.commit();
    }

    public static String getSharedPreference(Context context,String key) {
        SharedPreferences settings=context.getSharedPreferences(Config.SHARED_PREF_CONFIG, Context.MODE_PRIVATE);
        String text;
        text = settings.getString(key, null);
        return text;
    }

    public static void clearSharedPreference(Context context) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(Config.SHARED_PREF_CONFIG, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.clear();
        editor.commit();
    }
}
