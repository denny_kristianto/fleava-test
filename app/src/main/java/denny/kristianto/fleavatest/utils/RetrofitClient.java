package denny.kristianto.fleavatest.utils;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * FleavaTest
 * Created by Denny Kristianto on 07/11/2018.
 * Copyright (c) 2018 by Denny Kristianto
 */
public class RetrofitClient {
    private static Retrofit retrofit = null;
    private static Retrofit retrofit2=null;
    final static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(360, TimeUnit.SECONDS)
            .connectTimeout(360, TimeUnit.SECONDS)
            .build();

    public static Retrofit getClient(String server){
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(server)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getClient2(String server){
        if (retrofit2==null) {
            retrofit2 = new Retrofit.Builder()
                    .baseUrl(server)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit2;
    }
}
