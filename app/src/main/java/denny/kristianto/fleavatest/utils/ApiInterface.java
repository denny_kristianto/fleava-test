package denny.kristianto.fleavatest.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import denny.kristianto.fleavatest.model.MockModel;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

/**
 * FleavaTest
 * Created by Denny Kristianto on 06/11/2018.
 * Copyright (c) 2018 by Denny Kristianto
 */
public interface ApiInterface {
    @POST("/sign-in")
    Call<ResponseBody> login(@Body HashMap<String,String> loginInfo);

    @GET("/secret")
    Call<ResponseBody> checkLoginStatus(@HeaderMap Map<String, String> header);

    @GET(Config.MOCK_ADDRESS)
    Call<ArrayList<MockModel>> getMockData();

}
