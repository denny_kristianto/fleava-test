package denny.kristianto.fleavatest;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import denny.kristianto.fleavatest.adapter.MainAdapter;
import denny.kristianto.fleavatest.model.MockModel;
import denny.kristianto.fleavatest.utils.StaticUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InnerFragment extends Fragment {
    private Context context;
    private View view;
    private Unbinder unbinder;
    private ArrayList<MockModel> mockModels;
    private MainAdapter mainAdapter;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.title) TextView title;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_inner_fragment, container, false);
        context=getActivity();
        unbinder=ButterKnife.bind(this,view);
        assert getArguments() != null;
        title.setText(getArguments().getString("title"));
        initData();
        initRecyclerView();
        return view;
    }

    private void initData(){
        StaticUtils.mockInterface.getMockData().enqueue(new Callback<ArrayList<MockModel>>() {
            @Override
            public void onResponse(Call<ArrayList<MockModel>> call, Response<ArrayList<MockModel>> response) {
                mockModels=response.body();
                mainAdapter.addData(mockModels);
            }

            @Override
            public void onFailure(Call<ArrayList<MockModel>> call, Throwable t) {

            }
        });
    }

    private void initRecyclerView(){
        mockModels=new ArrayList<>();
        mainAdapter=new MainAdapter(context,mockModels);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(mainAdapter);
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        unbinder.unbind();
    }
}
