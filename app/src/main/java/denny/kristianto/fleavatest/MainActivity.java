package denny.kristianto.fleavatest;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import denny.kristianto.fleavatest.utils.StaticUtils;

public class MainActivity extends AppCompatActivity {
    private Context context;
    @BindView(R.id.tab_layout) TabLayout tabLayout;
    @BindView(R.id.floating_btn) ImageButton floatingBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        context=this;
        initTabLayout();
        floatingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,"You have clicked me :)",Toast.LENGTH_SHORT).show();
            }
        });
        tabLayout.getTabAt(2).select();
    }

    private void initTabLayout(){
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.body_icon).setText("Body"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.mind_icon).setText("Mind"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.spirit_icon).setText("Spirit"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.event_icon).setText("Event"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.body_icon));
        View view=LayoutInflater.from(context).inflate(R.layout.custom_view_profile,null);

        Objects.requireNonNull(tabLayout.getTabAt(4)).setCustomView(view);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()){
                    case 0:
                        changeFragmentTo("Body");
                        break;
                    case 1:
                        changeFragmentTo("Mind");
                        break;
                    case 2:
                        changeFragmentTo("Spirit");
                        break;
                    case 3:
                        changeFragmentTo("Event");
                        break;
                    case 4:
                        logoutDialog();
                        tabLayout.getTabAt(2).select();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void changeFragmentTo(String text){
        Fragment fragment = new InnerFragment();
        Bundle bundle=new Bundle();
        bundle.putString("title",text);
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrame, fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void logoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String uname=StaticUtils.getSharedPreference(context,"name");
        builder.setTitle("Logout ?");
        builder.setMessage(uname+ ", are you sure want to logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                StaticUtils.clearSharedPreference(context);
                Intent intent=new Intent(context,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton("Nope", null);
        builder.show();
    }


}
