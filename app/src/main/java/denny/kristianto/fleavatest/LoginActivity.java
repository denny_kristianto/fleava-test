package denny.kristianto.fleavatest;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.fb_login_btn) CardView fbLogin;
    @BindView(R.id.vk_login_btn) CardView vkLogin;
    @BindView(R.id.loginBtn) CardView loginBtn;
    @BindView(R.id.signupBtn) CardView signupBtn;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        context=this;
        initButtons();
    }

    private void initButtons(){
        fbLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,"Facebook login isn't available right now :')",Toast.LENGTH_SHORT).show();
            }
        });
        vkLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,"VK login isn't available right now :')",Toast.LENGTH_SHORT).show();
            }
        });
        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,"Signup for new account isn't available right now :')",Toast.LENGTH_SHORT).show();
            }
        });
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,LoginForm.class);
                startActivity(intent);
            }
        });
    }
}
