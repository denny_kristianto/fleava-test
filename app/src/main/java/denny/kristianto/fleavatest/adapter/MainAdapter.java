package denny.kristianto.fleavatest.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

import denny.kristianto.fleavatest.R;
import denny.kristianto.fleavatest.model.MockModel;

/**
 * FleavaTest
 * Created by Denny Kristianto on 07/11/2018.
 * Copyright (c) 2018 by Denny Kristianto
 */
    public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MainHolder> {
    private Context context;
    private ArrayList<MockModel> mockModels;

    public MainAdapter(Context context, ArrayList<MockModel> mockModels) {
        this.context = context;
        this.mockModels = mockModels;
    }

    public void addData(ArrayList<MockModel> data){
        mockModels.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MainHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v=LayoutInflater.from(context).inflate(R.layout.card_items,viewGroup,false);
        return new MainHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MainHolder mainHolder, int i) {
        mainHolder.genre.setText(mockModels.get(i).genre);
        mainHolder.item.setText(mockModels.get(i).item);
        mainHolder.name.setText(mockModels.get(i).name);
        Glide.with(context).load(mockModels.get(i).photo).into(mainHolder.photo);
        Glide.with(context).load(mockModels.get(i).profile_pict).into(mainHolder.profile_pict);
    }

    @Override
    public int getItemCount() {
        return mockModels.size();
    }

    class MainHolder extends RecyclerView.ViewHolder{
        TextView name, item, genre;
        ImageView photo;CircularImageView profile_pict;
        MainHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.content_name);
            item=itemView.findViewById(R.id.content_item);
            genre=itemView.findViewById(R.id.content_genre);
            photo=itemView.findViewById(R.id.content_photo);
            profile_pict=itemView.findViewById(R.id.content_profile_pict);
        }
    }
}
