package denny.kristianto.fleavatest.model;

import java.io.Serializable;

/**
 * FleavaTest
 * Created by Denny Kristianto on 07/11/2018.
 * Copyright (c) 2018 by Denny Kristianto
 */
public class MockModel implements Serializable {
    public Integer id;
    public String name;
    public String photo;
    public String profile_pict;
    public String genre;
    public String item;
}
