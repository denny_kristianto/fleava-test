package denny.kristianto.fleavatest;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import denny.kristianto.fleavatest.utils.StaticUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginForm extends AppCompatActivity {
    private Context context;
    @BindView(R.id.backBtn) CardView backBtn;
    @BindView(R.id.loginBtn) CardView signinBtn;
    @BindView(R.id.email) EditText email;
    @BindView(R.id.password) EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_form);
        context=this;
        ButterKnife.bind(this);
        init();
    }

    private void init(){
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        signinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String,String> map=new HashMap<>();
                map.put("email",email.getText().toString());
                map.put("password",password.getText().toString());
                StaticUtils.apiInterface.login(map).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful()){
                            try {
                                JSONObject object=new JSONObject(response.body().string());
                                if(object.has("token")){
                                    StaticUtils.writeSharedPreference(context,"email",object.getString("email"));
                                    StaticUtils.writeSharedPreference(context,"name",object.getString("name"));
                                    StaticUtils.writeSharedPreference(context,"token",object.getString("token"));
                                    StaticUtils.writeSharedPreference(context,"status",object.getString("is_active"));
                                    Intent intent=new Intent(context, MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }else {
                                    Toast.makeText(context,"Username / password is invalid",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            Toast.makeText(context,"Username / password is invalid",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(context,"Something is not going right",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
